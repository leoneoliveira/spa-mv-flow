import {HTTP} from '@/common.js';

export default class UsuarioService {
  lista () {
    return HTTP.get('/usuarios')
  }

  cadastra (usuario) {
    return HTTP.post('/usuarios', usuario)
  }

  atualiza (usuario) {
    return HTTP.put('/usuarios/' + usuario.id, usuario)
  }

  busca (id) {
    return HTTP.get('/usuarios/' + id);
  }
}
