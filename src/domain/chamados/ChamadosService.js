import { HTTP } from '@/common.js';

export default class ChamadosService {
  lista () {
    return HTTP.get('/chamados')
  }

  listaByUsuario (id) {
    return HTTP.get('/chamados/usuario/' + id)
  }

  cadastra (chamado) {
    return HTTP.post('/chamados', chamado)
  }
}
