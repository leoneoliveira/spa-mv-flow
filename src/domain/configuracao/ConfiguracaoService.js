/* eslint-disable */
import {HTTP} from '@/common.js';

export default class ConfiguracaoService {

    atualiza (configuracao) {
      return HTTP.put('/configuracoes/' + configuracao.cd_configuracao, configuracao);
    }

    busca (id) {
      return HTTP.get('/configuracoes/' + id);
    }
  }
