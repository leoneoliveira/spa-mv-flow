import axios from 'axios'
import store from '@/vuex/store'

export const HTTP = axios.create({
  baseURL: 'http://18.228.14.140:8000/RepMed',
  headers: {
    Authorization: store.state.tokenvue[0],
    'Content-Type': 'application/json',
    'Charset': 'utf-8'
  }
});
