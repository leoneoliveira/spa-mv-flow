export default {

  'LOAD_USER' (state, payload) {
    state.user = payload;
  },
  'UPDATE_USER' (state, payload) {
    state.user.nomeusuario = payload.nomeusuario;
    state.user.fotousuario = payload.fotousuario;
  },
  'UPDATE_CALENDARIO' (state, data) {
    state.dataCalendario = data;
  },
  'LOGIN' (state, token) {
    state.isLoggedIn = true;
    state.tokenvue = token;
  },
  'LOGOUT' (state) {
    state.isLoggedIn = false;
    state.tokenvue = [];
    state.user = [];
  }
}