export default {
  user: {
    idusuario: null,
    nomeusuario: null,
    fotousuario: null
  },
  dataCalendario: new Date(),
  isLoggedIn: false,
  tokenvue: []
}
