// User
import User from '@/pages/users/User'
// Login
import Login from '@/pages/login/Login'
import EsqueceuSenha from '@/pages/login/EsqueceuSenha'
import Registrar from '@/pages/login/Registrar'

// Chamados
import Chamados from '@/pages/chamados/Chamados'

// Notificação
import Configuracoes from '@/pages/configuracoes/Configuracoes'

// import Guard from '../services/middleware'

// telas workflow
import Workflow from '@/pages/Workflow/Workflow'
import Historico from '@/pages/Workflow/Historico'
import Producao from '@/pages/Workflow/Producao'
import Procedimento from '@/pages/Workflow/Procedimento'
import Procedimentodata from '@/pages/Workflow/Procedimento-data'

import store from '@/vuex/store'

function requireAuth (to, from, next) {
  if (!store.state.isLoggedIn) { /// THIS NOT WORK, HOW TO ACCESS STORE?
    window.location.href = '/'
  } else {
    next()
  }
}

export const routes = [
  {
    path: '/home',
    name: 'home',
    component: Workflow,
    titulo: 'Home',
    icon: 'fa fa-home',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '/Workflow',
    titulo: 'Workflow',
    menu: true,
    submenu: true,
    beforeEnter: requireAuth,
    icon: 'fa fa-map-o',
    subRoutes: [
      {
        path: '/workflow/producao',
        component: Producao,
        titulo: 'Produção',
        name: 'producao',
        beforeEnter: requireAuth
      },
      {
        path: '/workflow/historico',
        component: Historico,
        titulo: 'Histórico',
        name: 'historico',
        beforeEnter: requireAuth
      }
    ]
  },
  {
    path: '/workflow/historico',
    name: 'historico',
    component: Historico,
    titulo: 'Historico'
  },
  {
    path: '/workflow/producao',
    name: 'producao',
    component: Producao,
    titulo: 'Produção',
    icon: 'fa fa-map-o',
    beforeEnter: requireAuth
  },
  {
    path: '/workflow/producao/procedimento',
    name: 'procedimento',
    component: Procedimento,
    titulo: 'procedimento'
  },
  {
    path: '/workflow/producao/procedimento/data',
    name: 'procedimento-data',
    component: Procedimentodata,
    titulo: 'procedimento'
  },
  {
    path: '/usuario',
    name: 'Usuario',
    component: User,
    titulo: 'Usuario',
    icon: 'fa fa-user',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    titulo: 'Login',
    menu: false
  },
  {
    path: '/esqueceusenha',
    name: 'EsqueceuSenha',
    component: EsqueceuSenha,
    titulo: 'EsqueceuSenha',
    menu: false
  },
  {
    path: '/registrar',
    name: 'Registrar',
    component: Registrar,
    titulo: 'Registrar',
    menu: false
  },
  {
    path: '/chamados',
    name: 'chamados',
    component: Chamados,
    titulo: 'Chamados',
    icon: 'fa fa-envelope-o',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '/configuracoes',
    name: 'configuracoes',
    component: Configuracoes,
    titulo: 'Configuracoes',
    icon: 'fa fa-cog',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '*',
    component: Login,
    menu: false
  }
]
