// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import { routes } from '@/router/router'
import VeeValidate from 'vee-validate'
import msg from './pt_BR';
import BootstrapVue from 'bootstrap-vue'
import vSelect from 'vue-select';
import Vuex from 'vuex'
import store from './vuex/store'
import { sync } from 'vuex-router-sync'
import VueCharts from 'vue-chartjs'

Vue.component('v-select', vSelect);
// import Inputmask from 'inputmask';
const VueInputMask = require('vue-inputmask').default

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueInputMask);
Vue.use(Vuex);

Vue.use(VeeValidate, {
  locale: 'pt_BR',
  dictionary: {
    pt_BR: {
      messages: msg
    }
  },
  fieldsBagName: 'veeFields'
});

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.use(VueCharts);

Vue.config.productionTip = false

sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app-mv',
  store,
  router,
  components: { App },
  template: '<App/>'
});
