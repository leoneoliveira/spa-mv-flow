import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  mounted () {
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho'],
      datasets: [
        {
          label: 'Repassado',
          backgroundColor: '#4D7498',
          data: [32000, 20000, 12000, 39000, 10000, 40000, 3000]
        }
      ]
    })
  }
}